# README #

Subscribes to a IoT core MQTT topic and prints incoming messages from the topic.

## Build project :
`./gradle build`

## Run:
Execute the following command:
`java -jar iot-subscribe-1.0-SNAPSHOT.jar <local-cert-folder-path> <topic_name> <aws_endpoint>`

| Params | description |
|--|--|
| "local-cert-folder-path" | ex: C:\abcd\ or \user\abcd\ |
| "topic_name" | ex: topic name in AWS IoT Core |
| "aws_endpoint" | AWS endpoint in IoT core settings |
