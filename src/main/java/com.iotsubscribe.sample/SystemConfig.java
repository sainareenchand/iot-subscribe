package com.iotsubscribe.sample;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class SystemConfig {
    public static final String OS_NAME = "os.name";
    public static final String OS_VERSION = "os.version";
    public static final String OS_ARCH = "os.arch";
    private int messageId;
    private String osName;
    private String osVersion;
    private String osArch;

    public SystemConfig() {
        this.osName = System.getProperty(OS_NAME);
        this.osVersion = System.getProperty(OS_VERSION);
        this.osArch = System.getProperty(OS_ARCH);

    }

    public int getMessageId() {
        return messageId;
    }

    public String getOsName() {
        return osName;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public String getOsArch() {
        return osArch;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    @Override
    public String toString() {
        return "SystemConfig{" +
                "messageId=" + messageId +
                ", osName='" + osName + '\'' +
                ", osVersion='" + osVersion + '\'' +
                ", osArch='" + osArch + '\'' +
                '}';
    }
}