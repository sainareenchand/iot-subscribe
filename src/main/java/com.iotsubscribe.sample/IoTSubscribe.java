package com.iotsubscribe.sample;

import com.fasterxml.jackson.databind.ObjectMapper;
import software.amazon.awssdk.crt.CRT;
import software.amazon.awssdk.crt.io.ClientBootstrap;
import software.amazon.awssdk.crt.io.EventLoopGroup;
import software.amazon.awssdk.crt.io.HostResolver;
import software.amazon.awssdk.crt.mqtt.MqttClientConnection;
import software.amazon.awssdk.crt.mqtt.MqttClientConnectionEvents;
import software.amazon.awssdk.crt.mqtt.QualityOfService;
import software.amazon.awssdk.iot.AwsIotMqttConnectionBuilder;

import java.io.UnsupportedEncodingException;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class IoTSubscribe {

    private String clientId = "demo-" + UUID.randomUUID().toString();

    public static void main(String[] args) {
        IoTSubscribe topicSubscribe = new IoTSubscribe();
        String endpoint = args[2];
        String topic = args[1];
        String rootCaPath = args[0] + "RootCA.pem";
        String certPath = args[0] + "certificate.pem.crt";
        String keyPath = args[0] + "private.pem.key";
        topicSubscribe.subscribeToMqttTopic(rootCaPath, certPath, keyPath, topic, endpoint);
    }

    private void subscribeToMqttTopic(String rootCaPath, String certPath, String keyPath, String topic, String endpoint) {
        ObjectMapper mapper = new ObjectMapper();

        try (EventLoopGroup eventLoopGroup = new EventLoopGroup(1);
             HostResolver resolver = new HostResolver(eventLoopGroup);
             ClientBootstrap clientBootstrap = new ClientBootstrap(eventLoopGroup, resolver);
             AwsIotMqttConnectionBuilder mqttConnectionBuilder = AwsIotMqttConnectionBuilder.newMtlsBuilderFromPath(certPath, keyPath)) {

            MqttClientConnectionEvents callbacks = new MqttClientConnectionEvents() {
                @Override
                public void onConnectionInterrupted(int errorCode) {
                    if (errorCode != 0) {
                        System.out.println("Connection interrupted: " + errorCode + ": " + CRT.awsErrorString(errorCode));
                    }
                }

                @Override
                public void onConnectionResumed(boolean sessionPresent) {
                    System.out.println("Connection resumed: " + (sessionPresent ? "existing session" : "clean session"));
                }
            };

            mqttConnectionBuilder.withBootstrap(clientBootstrap)
                    .withCertificateAuthorityFromPath(null, rootCaPath)
                    .withConnectionEventCallbacks(callbacks)
                    .withClientId(clientId)
                    .withEndpoint(endpoint)
                    .withCleanSession(true);

            try (MqttClientConnection connection = mqttConnectionBuilder.build()) {

                // Connect to MQTT Broker
                CompletableFuture<Boolean> connected = connection.connect();
                try {
                    boolean sessionPresent = connected.get();
                    System.out.println("Connected to " + (!sessionPresent ? "new" : "existing") + " session!");
                } catch (Exception ex) {
                    throw new RuntimeException("Exception occurred during connect", ex);
                }

                // Subscribe to topic.
                CompletableFuture<Integer> subscribed = connection.subscribe(topic, QualityOfService.AT_LEAST_ONCE, (mqttMessage -> {
                    try {
                        String payload = new String(mqttMessage.getPayload(), "UTF-8");
                        System.out.println("Message received: " + payload);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }));
                subscribed.get();
                Scanner scanner = new Scanner(System.in);
                scanner.next();
                CompletableFuture<Void> disconnected = connection.disconnect();
                disconnected.get();

            } catch (Exception ex) {
                System.out.println("Error" + ex);
            }
        }
    }

}
